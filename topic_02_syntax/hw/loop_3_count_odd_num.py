"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""


def count_odd_num(natur):
    a = 0
    if type(natur) != int:
        return 'Must be int!'
    elif natur < 1:
        return 'Must be > 0!'
    else:
        if natur > 0:
            while natur > 0:
                must = natur % 10
                natur = natur // 10
                if (must % 2) == 1:
                    a = a + 1

        return a
