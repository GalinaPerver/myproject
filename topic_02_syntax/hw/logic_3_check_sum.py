"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    if (a + b == c) or (b + c == a) or (c + a == b):
        return True
    else:
        return False
