"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    my_sum = 0
    for my_symbol in range(1, 113, 3):
        my_sum += int(my_symbol)

    return my_sum
