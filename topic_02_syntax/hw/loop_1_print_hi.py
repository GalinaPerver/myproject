"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Если число <= 0, тогда вывести пустую строку.
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi(n):

    if n > 0:
        for i in range(n):
            print('Hi, friend!', end='')



    else:
        print("")

    if n > 0:

        print('')
