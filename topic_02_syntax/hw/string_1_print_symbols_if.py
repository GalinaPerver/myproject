"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(string):
    dlina = len(string)

    if not string:
        print("Empty string!")

    elif dlina > 5:
        num1 = string[0:3]
        num2 = string[-3:]
        print(num1, end='')
        print(num2)
    elif dlina < 6:
        print(string[0] * dlina)
