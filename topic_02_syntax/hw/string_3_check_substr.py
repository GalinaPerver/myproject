"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""

def check_substr(str1, str2):
    num1 = len(str1)
    num2 = len(str2)
    if num1 == num2:
        return False
    elif num1 == 0 or num2 == 0:
        return True
    if str1 in str2:
        return True
    if str2 in str1:
        return True
    else:
        return False
