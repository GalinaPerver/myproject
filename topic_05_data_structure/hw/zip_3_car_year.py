"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(lst: list, year: list):
    if (type(lst) != list) or (type(year) != list):
        return 'Must be list!'
    if (len(lst) == 0) or (len(year) == 0):
        return 'Empty list!'
    else:
        ex = list(zip_longest(lst, year, fillvalue='???'))
        return ex
