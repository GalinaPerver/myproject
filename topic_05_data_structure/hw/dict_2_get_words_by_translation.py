"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(rueng, ru):
    if type(rueng) != dict:
        return "Dictionary must be dict!"
    if type(ru) != str:
        return "Word must be str!"

    if len(rueng) == 0:
        return "Dictionary is empty!"
    if len(ru) == 0:
        return "Word is empty!"
    else:
        lst = []
        for cool, go in rueng.items():
            if ru in go:
                lst.append(cool)

        if len(lst) > 0:
            return lst
        else:
            return f"Can't find English word: {ru}"
